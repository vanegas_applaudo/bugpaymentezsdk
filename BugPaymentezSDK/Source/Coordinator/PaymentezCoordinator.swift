//
//  PaymentezCoordinator.swift
//  BugPaymentezSDK
//
//  Created by Applaudo on 3/2/21.
//

import Foundation
import PaymentezSDK

protocol PaymentezCoordinatorType: class {
    var buyer: PmzBuyer { get set }
    var style: PmzStyle { get set }
    
    var onSearchFinished: ((PmzOrder) -> Void)? { get set }
    var onSearchCancelled: (() -> Void)? { get set }
    var onPayAndPlaceFinished: ((PmzOrder) -> Void)? { get set }
    var onGetStores: (([PmzStore]) -> Void)? { get set }
    var onError: ((PmzOrder?, String?) -> Void)? { get set }
    
    func start(on navigationController: UINavigationController?)
    func startBySearch(_ commerceName: String, on navigationController: UINavigationController?)
    func reopenOrder(_ order: PmzOrder, on navigationController: UINavigationController?)
}

class PaymentezCoordinator: PaymentezCoordinatorType {
    // Properties
    var buyer: PmzBuyer = PmzBuyer()
    var style: PmzStyle = PmzStyle()
    
    // Callbacks
    var onSearchFinished: ((PmzOrder) -> Void)?
    var onSearchCancelled: (() -> Void)?
    var onPayAndPlaceFinished: ((PmzOrder) -> Void)?
    var onGetStores: (([PmzStore]) -> Void)?
    var onError: ((PmzOrder?, String?) -> Void)?
    
    func start(on navigationController: UINavigationController?) {
        guard let navigation = navigationController else { return }
        
        PaymentezSDK.shared
            .setStyle(style: style)
            .startSearch(navigationController: navigation, buyer: buyer, appOrderReference: "appOrderReference", callback: self)
    }
    
    func startBySearch(_ commerceName: String, on navigationController: UINavigationController?) {
        guard let navigation = navigationController else { return }
        
        PaymentezSDK.shared
            .setStyle(style: style)
            .startSearch(navigationController: navigation, buyer: buyer, appOrderReference: "appOrderReference", searchStoresFilter: commerceName, callback: self)
    }
    
    func reopenOrder(_ order: PmzOrder, on navigationController: UINavigationController?) {
        guard let navigation = navigationController else { return }

        PaymentezSDK.shared
            .reopenOrder(navigationController: navigation, order: order, buyer: buyer, appOrderReference: "appOrderReference", callback: self)
    }
}

extension PaymentezCoordinator: PmzSearchCallback {
    func searchFinishedSuccessfully(order: PmzOrder) {
        onSearchFinished?(order)
    }
    
    func searchFinishedWithError(error: PmzError) {
        onError?(nil, error.errorMessage)
    }
    
    func searchCancelled() {
        onSearchCancelled?()
    }
}

extension PaymentezCoordinator: PmzPayAndPlaceCallback {
    func payAndPlaceFinishedSuccessfully(order: PmzOrder) {
        onPayAndPlaceFinished?(order)
    }
    
    func payAndPlaceOnError(order: PmzOrder?, error: PmzError) {
        onError?(order, error.errorMessage)
    }
}

extension PaymentezCoordinator: PmzGetStoresCallback {
    func getStoresOnSuccess(stores: [PmzStore]) {
        onGetStores?(stores)
    }
    
    func getStoresOnError(error: PmzError) {
        onError?(nil, error.errorMessage)
    }
}
