//
//  SDKViewController.swift
//  BugPaymentezSDK
//
//  Created by Applaudo on 3/2/21.
//

import UIKit
import PaymentezSDK
import TinyConstraints

class SDKViewController: UIViewController {
    // Properties
    private var paymentezCoordinator: PaymentezCoordinatorType = PaymentezCoordinator()
    private var shouldStartSDK = true
    private var pmzOrder: PmzOrder?
    
    lazy var orderLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 0
        
        return label
    }()
    
    lazy var backbutton: UIButton = {
        let button = UIButton()
        button.setTitle("Reopen Order", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.addTarget(self, action: #selector(reopenOrder), for: .touchUpInside)
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupNavigationViews()
        setupSDKCoordinator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        renderViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // MARK: If there's no order and hasn't started the SDK yet. Start the SDK
        if pmzOrder == nil, shouldStartSDK {
            startOrderAhead()
            return
        }
        
        // MARK: If there's no order and the SDK has already started, that means the user wants to go back. Pop ViewController
        if pmzOrder == nil {
            navigationController?.popViewController(animated: true)
        }        
    }
    
    private func setupViews() {
        let stack = UIStackView(arrangedSubviews: [backbutton, orderLabel])
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillEqually
        view.backgroundColor = .white
        
        view.addSubview(stack)
        
        stack.centerXToSuperview()
        stack.centerYToSuperview()
    }
    
    private func setupNavigationViews() {
        navigationItem.hidesBackButton = true
    }
    
    private func renderViews() {
        orderLabel.text = pmzOrder?.store?.commerceFiscalNumber
        
        // Hides views when there's no placed order
        [backbutton, orderLabel].forEach { $0.isHidden = pmzOrder == nil }
    }
    
    // MARK: Setup Coordinator
    private func setupSDKCoordinator() {
        let buyer = PmzBuyer().setName("Pepe").setPhone("123123123").setFiscalNumber("fiscalNumber").setUserReference("userReference").setEmail("pepe@test.com.ar")
        let style = PmzStyle().setBackgroundColor(.gray).setButtonBackgroundColor(.red).setButtonTextColor(.white).setTextColor(.black)
        
        paymentezCoordinator.buyer = buyer
        paymentezCoordinator.style = style
        paymentezCoordinator.onSearchFinished = processOrder
        paymentezCoordinator.onSearchCancelled = searchCancelled
        paymentezCoordinator.onError = { [weak self] _, _ in
            // Clean view
            self?.shouldStartSDK = false
            self?.pmzOrder = nil
            self?.navigationController?.popViewController(animated: true)
        }
    }
}

// MARK: Coordinator implementation
extension SDKViewController {
    @objc private func reopenOrder() {
        guard let order = pmzOrder else { return }
        
        // Clean view
        shouldStartSDK = false
        pmzOrder = nil
        
        renderViews()
        paymentezCoordinator.reopenOrder(order, on: navigationController)
    }
    
    private func startOrderAhead() {
        paymentezCoordinator.startBySearch("La Brasa Roja", on: navigationController)
    }
    
    private func processOrder(_ order: PmzOrder) {
        pmzOrder = order
        renderViews()
    }
    
    private func searchCancelled() {
        shouldStartSDK = false
    }
}
