//
//  InitialViewController.swift
//  BugPaymentezSDK
//
//  Created by Applaudo on 3/2/21.
//

import UIKit
import TinyConstraints

class InitialViewController: UIViewController {
    lazy var openSDKButton: UIButton = {
        let button = UIButton()
        button.setTitle("Open SDK", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.addTarget(self, action: #selector(openOrderAhead), for: .touchUpInside)
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupViews()
    }
    
    @objc private func openOrderAhead() {
        let viewController = SDKViewController()
        
        /**
         No deberia de tener relacion ya que son dos pantallas bien diferentes.
         Sin embargo el SDK toma el navigation controller, dentro de el pueda que algo esten haciendo que este generando el issue.
         */
        
        
        // MARK: AQUI se produce el bug, cuando animated es "false" (Probar con true, el SDK no presenta bugs)
        navigationController?.pushViewController(viewController, animated: false)
    }
    
    private func setupViews() {
        view.addSubview(openSDKButton)
        
        openSDKButton.centerYToSuperview()
        openSDKButton.centerXToSuperview()
    }
}

