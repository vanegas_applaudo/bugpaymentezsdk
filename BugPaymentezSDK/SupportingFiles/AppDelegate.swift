//
//  AppDelegate.swift
//  BugPaymentezSDK
//
//  Created by Applaudo on 3/2/21.
//

import UIKit
import PaymentezSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // MARK: Setup PaymentezSDK
        PaymentezSDK.shared.initialize(appCode: "PMTZ-SDK-LM-CO-SERVER", appKey: "S72CVybhzWRTMFJHnyLLMJS0cXXRpQ4")
        
        // MARK: Setup initial view
        window = UIWindow(frame: UIScreen.main.bounds)
        
        window?.rootViewController = UINavigationController(rootViewController: InitialViewController())
        window?.makeKeyAndVisible()
        
        return true
    }
}

